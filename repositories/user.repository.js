const fighters = [
  {
    "_id": "1",
    "name": "Ryu",
    "health": 45, 
    "attack": 4, 
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
  },
  {
    "_id": "2",
    "name": "Dhalsim",
    "health": 60, 
    "attack": 3, 
    "defense": 1,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/dhalsim-hdstance.gif"
  },
  {
    "_id": "3",
    "name": "Guile",
    "health": 45, 
    "attack": 4, 
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/guile-hdstance.gif"
  },
  {
    "_id": "4",
    "name": "Zangief",
    "health": 60, 
    "attack": 4, 
    "defense": 1,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/zangief-hdstance.gif"
  },
  {
    "_id": "5",
    "name": "Ken",
    "health": 45, 
    "attack": 3, 
    "defense": 4,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ken-hdstance.gif"
  },
  {
    "_id": "6",
    "name": "Bison",
    "health": 45, 
    "attack": 5, 
    "defense": 4,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif"
  },
  {
    "_id": "7",
    "name": "Chun-Li",
    "health": 40, 
    "attack": 3, 
    "defense": 8,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/chunli-hdstance.gif"
  },
  {
    "_id": "8",
    "name": "Blanka",
    "health": 80, 
    "attack": 8, 
    "defense": 2,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/blanka-hdstance.gif"
  },
  {
    "_id": "9",
    "name": "E.Honda",
    "health": 50, 
    "attack": 5, 
    "defense": 5,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ehonda-hdstance.gif"
  },
  {
    "_id": "10",
    "name": "Balrog",
    "health": 55, 
    "attack": 4, 
    "defense": 6,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/balrog-hdstance.gif"
  },
  {
    "_id": "11",
    "name": "Vega",
    "health": 50, 
    "attack": 4, 
    "defense": 7,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/vega-hdstance.gif"
  },
  {
    "_id": "12",
    "name": "Sagat",
    "health": 55, 
    "attack": 4, 
    "defense": 6,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/sagat-hdstance.gif"
  },
  {
    "_id": "13",
    "name": "Cammy",
    "health": 45, 
    "attack": 4, 
    "defense": 7,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/cammy-hdstance.gif"
  },
  {
    "_id": "14",
    "name": "Fei Long",
    "health": 80, 
    "attack": 2, 
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/feilong-hdstance.gif"
  },
  {
    "_id": "15",
    "name": "Dee Jay",
    "health": 50, 
    "attack": 5, 
    "defense": 5,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/deejay-hdstance.gif"
  },
  {
    "_id": "16",
    "name": "T.Hawk",
    "health": 60, 
    "attack": 5, 
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/thawk-hdstance.gif"
  },
  {
    "_id": "17",
    "name": "Akuma",
    "health": 70, 
    "attack": 5, 
    "defense": 5,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/akuma-hdstance.gif"
  }
];

const saveData = (data, id) => {
  if (data) {
  	if (id) {
  		for (let i = 0; i < fighters.length; i++) {
			if (fighters[i]._id == id) {
				for (let key in data) {
		  			if (key == "name") fighters[i][key] = data[key][0].toUpperCase() + data[key].substr(1).toLowerCase();
		  			else fighters[i][key] = data[key];
		  		}
			}
		}
  	} else {
  		if (!data._id) data._id = String(+fighters[fighters.length - 1]._id + 1);
	    data.name = data.name[0].toUpperCase() + data.name.substr(1).toLowerCase();
	    fighters.push(data);
	    return data._id;
  	}
  } else {
    return false;
  }
};

const getData = (data) => {
	if (data) {
		for (let i = 0; i < fighters.length; i++) {
			if (fighters[i]._id == data) return fighters[i];
		}
		return false;
	} else {
		return fighters;
	}
};

const dataExistance = (data) => {
	if (data) {
		for (let i = 0; i < fighters.length; i++) {
			if (fighters[i].name.toLowerCase() == data.name.toLowerCase()) return false;
		}
		return true;
	} else {
		return false;
	}
};

const deleteData = (id) => {
	if (id) {
		for (let i = 0; i < fighters.length; i++) {
			if (fighters[i]._id == id) fighters.splice(i, 1);
		}
	} else {
		return false;
	}
}

module.exports = {
  saveData,
  getData,
  deleteData,
  dataExistance
};