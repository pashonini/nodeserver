const isAuthorized = (req, res, next) => {
  if (
    req &&
    req.headers &&
    req.headers.authorization &&
    req.headers.authorization === 'admin'
  ) {
    next();
  } else {
    res.status(401).send("You should have admin rights for this action!");
  }
};

module.exports = {
  isAuthorized
}