const { saveData } = require("../repositories/user.repository");

const userValidator = (user) => {
  if (user.name && user.health && user.attack && user.defense) {
    return true;
  } else {
    return false;
  }
};

const typeValidator = (user) => {
  for (let key in user) {
    switch (key) {
      case "_id":
        if (typeof user[key] != 'string') return false;
        break;
      case "name":
        if (typeof user[key] != 'string') return false;
        break;
      case "health":
        if (typeof user[key] != 'number') return false;
        break;
      case "attack":
        if (typeof user[key] != 'number') return false;
        break;
       case "defense":
        if (typeof user[key] != 'number') return false;
        break;
       case "source":
        if (typeof user[key] != 'string') return false;
        break;
    }
  }
  return true;
};

const allowedProps = (user) => {
  for (let key in user) {
    if (key != "name" && key != "health" && key != "attack" && key != "defense") return false;
  }
  return true;
};

module.exports = {
  userValidator,
  typeValidator,
  allowedProps
};