var express = require('express');
var router = express.Router();

const { userValidator, typeValidator, allowedProps } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");
const { saveData, getData, deleteData, dataExistance } = require("../repositories/user.repository");

router.get('/', function(req, res, next) {
  const result = getData();

  if (result) {
  	res.send(result);
  } else {
  	res.status(400).send(`Something went wrong!`);
  }
});

router.get('/:id', function(req, res, next) {
	const result = getData(req.params.id);

	if (result) {
		res.send(result);
	}	else {
  		res.status(400).send(`User not found!`);
  	}
});

router.post('/', isAuthorized, function(req, res, next) {
  const valid = userValidator(req.body);
  const type = typeValidator(req.body);

  if (valid && type) {
    if (dataExistance(req.body)) {
    	res.send(`User succesfully saved! New user's id: ${saveData(req.body)}`);
    } else {
    	res.status(400).send(`User you're trying to add, already exists!`);
    }
  } else {
    res.status(400).send(`Not valid request! It should contain at least 4 properties: name = 'string', health = 'number', attack = 'number', defense = 'number'`);
  }
});

router.put('/:id', isAuthorized, function(req, res, next){
	const result = getData(req.params.id);
	const type = typeValidator(req.body);
	const props = allowedProps(req.body);

	if (result) {
		if (props && type) {
			saveData(req.body, req.params.id);
			res.send(`User with id: ${req.params.id}, successfully edited!`);
		} else {
			res.status(400).send("Not valid request! Alowed to edit only properties: name = 'string', health = 'number', attack = 'number', defense = 'number'");
		}
	} else {
		res.status(400).send(`User you're trying to edit doesn't exist!`);
	}
});

router.delete('/:id', isAuthorized, function(req, res, next){
	const result = getData(req.params.id);

	if (result) {
		deleteData(req.params.id);
		res.send(`User with id: ${req.params.id}, successfully deleted!`);
	} else {
		res.status(400).send(`User you're trying to delete not found!`);
	}
});

module.exports = router;